# Project0- Console based Bank Application

## Description
This project creates a console based Bank Application. User can create account using Name, Email, Password and Phone Number. Once logged in User can create Checking account and Savings Account. Checking Account number will be identified using a randomly generated account number with first digit "1". Similarly Savings account number will be identified with first digit "0". User will be able to perform operations such as view savings/checking account balance, deposit cash to account, withdraw cash from account, transfer cash between savings and checking account, view transaction history.

## IDE
* IntelliJ

## Tech Stack
* Java 8
* Apache Maven
* PostgreSQL deployed on AWS RDS
* Git 




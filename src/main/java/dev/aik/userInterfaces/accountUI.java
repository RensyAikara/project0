package dev.aik.userInterfaces;

import dev.aik.daos.*;
import dev.aik.models.CheckingAccount;
import dev.aik.models.SavingsAccount;
import dev.aik.models.User;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class accountUI extends uiFunctions {
////Method to create new User Account with Name, Email, Password. Phone Number
    public static void newAccountCreation() {
        UserDao userDao = new UserDaoImpl();
        Scanner sc = new Scanner(System.in);
        System.out.println("----------------Create New User Account!!!!----------------------");
        System.out.println("Enter Name: ");
        String name = sc.nextLine();
        System.out.println("Enter email ID: ");
        String email = sc.nextLine();
        System.out.println("Enter password: ");
        String password = sc.nextLine();
        while(password.length()<8){
            System.out.println("Password should contain atleast 8 letters, try again: ");
            password = sc.nextLine();
        }
        System.out.println("Enter phone number: ");
        Long phoneNumber = sc.nextLong();
        User newUser = new User(name, email, phoneNumber, password);
        boolean result = false;
        String newUserEmail = newUser.getEmailId();
        String result1 = userDao.existingAccount(newUserEmail);
///if account already exists, it will go back to main menu
        if (result1 != ""){
            System.out.println("Account already found with Name: " +result1);
            System.out.println("--------Try Logging In or Create new account-----------");
            menuInitial();
        }
        else {
            result = userDao.createUserAccount(newUser);
            if (result == true) {
                System.out.println("-------Account created successfully!!!------------------");
///Once Account is created User can login to account using username and password
                System.out.println("Login with your username and password");
                String name1 = sc.nextLine();
                System.out.println("Press Enter");
                String password1 = sc.nextLine();


                System.out.println("Enter email: ");
                String emailIdEntered = sc.nextLine();
                String passwordSaved = userDao.checkPassword(emailIdEntered);
                System.out.println("Enter password: ");
                String passwordEntered = sc.nextLine();
                if(passwordEntered.equals(passwordSaved)){
                    System.out.println("Logged in successfully!!!");
                    User userDetails = userDao.accountDetails(emailIdEntered);
///Welcome message with user name printed
                    System.out.println("Hi " + userDetails.getName() + " Welcome to Rev Bank!!!");
                    int loggedInUserId = userDetails.getId();
                   // userDao.updateLoggedIn(loggedInUserId);
///Ask User whether to create savings account or checking account
                    int fifthChoice = menuFive();
                    SavingsAccountDao savingDao = new SavingsAccountDaoImpl();
                    CheckingAccountDao checkingDao = new CheckingAccountDaoImpl();
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SS");
                    LocalDateTime date = LocalDateTime.now();
                    String dateTime = dtf.format(date);
                    switch (fifthChoice) {
//////if user wants to create savings account
                        case 1:
///Generate Savings account number by calling generateSavingsAccountNumber method
                            String savingsAcNumber = savingDao.generateSavingsAccountNumber(phoneNumber);
                            System.out.println("Your Savings Account number is: " + savingsAcNumber);
                            double savingsBalance = 0,savingsDeposit = 0, savingsWithdraw = 0;
                            SavingsAccount newAccount = new SavingsAccount(savingsAcNumber,savingsBalance,savingsDeposit,savingsWithdraw,dateTime,loggedInUserId);
                            boolean result2 = savingDao.createSavingsAccount(newAccount);
                            if(result2 == true){
                                System.out.println("savings account created successfully...");
///Once account is created, calling a method which asks user about the operations to perform
                                savingsOperations(savingsAcNumber, loggedInUserId, dateTime);
                            }
                            break;
///////if user wants to create checking account
                        case 2:
///Generate Savings account number by calling generateCheckingAccountNumber method
                            String checkingAcNumber = checkingDao.generateCheckingAccountNumber(phoneNumber);
                            System.out.println("Your Checking Account number is: " + checkingAcNumber);
                            double checkingBalance = 0,checkingDeposit = 0, checkingWithdraw = 0;
                            CheckingAccount newCheckAccount = new CheckingAccount(checkingAcNumber,checkingBalance,checkingDeposit,checkingWithdraw,dateTime,loggedInUserId);
                            boolean result3 = checkingDao.createCheckingAccount(newCheckAccount);
                            if(result3 == true){
                                System.out.println("checking account created successfully...");
///Once account is created, calling a method which asks user about the operations to perform
                                checkingOperations(checkingAcNumber, loggedInUserId, dateTime);
                            }
                            break;
                    }
                }
                else if(passwordSaved == ""){
                    System.out.println("Account not found!! Try creating new account");
                    newAccountCreation();
                }
                else{
                    System.out.println("Wrong password");
                    accountOperations();
                }
            }
        }
    }
/// method accountOperations makes user to choose Checking account or Savings account
    public static void accountOperations(){
        Scanner sc = new Scanner(System.in);
        UserDao userDao = new UserDaoImpl();
        System.out.println("Login here!!!!");
        System.out.println("Enter email: ");
        String emailIdEntered = sc.nextLine();
        System.out.println("Enter password: ");
        String passwordEntered = sc.nextLine();
        String passwordSaved = userDao.checkPassword(emailIdEntered);
        if(passwordEntered.equals(passwordSaved)){
            User userDetails = userDao.accountDetails(emailIdEntered);
            Long phoneNumber = userDetails.getPhoneNumber();
            System.out.println("Hi " + userDetails.getName() + " Welcome to Rev Bank!!!");
            int loggedInUserId = userDetails.getId();   /////////////////////////////User Id of loggedin User
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SS");
            LocalDateTime date = LocalDateTime.now();
            String dateTime = dtf.format(date);

            SavingsAccountDao savingsAc = new SavingsAccountDaoImpl();
            CheckingAccountDao checkingAc = new CheckingAccountDaoImpl();
            int fourthchoice = menuFour();
            switch (fourthchoice){
                case 1: ///////Savings Account////////
                    String savAcNumber = savingsAc.savingsAccountFound(loggedInUserId);     ////////Savings A/C number of logged in User
                    if(savAcNumber == ""){
                        System.out.println("No savings account found .. do you want to create savings account? (Yes/No)");
                        String answer = sc.nextLine();
                        if(answer.equals("Yes")){
                            savAcNumber = savingsAc.generateSavingsAccountNumber(phoneNumber);
                            System.out.println("Your Savings Account number is: " + savAcNumber);
                            double savingsBalance = 0,savingsDeposit = 0, savingsWithdraw = 0;
                            SavingsAccount newAccount = new SavingsAccount(savAcNumber,savingsBalance,savingsDeposit,savingsWithdraw,dateTime,loggedInUserId);
                            savingsAc.createSavingsAccount(newAccount);
                            savingsOperations(savAcNumber, loggedInUserId, dateTime);
                        }
                    }
                    else{
                        savingsOperations(savAcNumber, loggedInUserId, dateTime);
                    }
                    break;
                case 2:  //////Checking Account//////
                    String checkAcNumber = checkingAc.checkingAccountFound(loggedInUserId);     ////////Savings A/C number of logged in User
                    if(checkAcNumber == ""){
                        System.out.println("No checking account found .. do you want to create checking account? (Yes/No)");
                        String answer = sc.nextLine();
                        if(answer.equals("Yes")){
                            checkAcNumber = checkingAc.generateCheckingAccountNumber(phoneNumber);
                            System.out.println("Your Checking Account number is: " + checkAcNumber);
                            double checkingBalance = 0,checkingDeposit = 0, checkingWithdraw = 0;
                            CheckingAccount newAccount = new CheckingAccount(checkAcNumber,checkingBalance,checkingDeposit,checkingWithdraw,dateTime,loggedInUserId);
                            checkingAc.createCheckingAccount(newAccount);
                            checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                        }
                    }
                    else{
                        checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                    }
                    break;
            }
        }
        else if(passwordSaved == ""){
            System.out.println("Account not found!! Try creating a new account");
            newAccountCreation();
        }
        else{
            System.out.println("Wrong password!!!! TRY AGAIN!!!!!!");
            menuInitial();
        }
    }

    public static int menuFive(){
        Scanner sc = new Scanner(System.in);
        int input;
        do{
            System.out.println("Which account do you want to create? ");
            System.out.println("1 - Savings");
            System.out.println("2 - Checking");
            input = sc.nextInt();
            if(input < 1 || input > 2){
                System.out.println("Invalid Entry!! TRY AGAIN!!");
            }
        }while(input < 1 || input > 2);
        return input;
    }

    public static int menuFour(){
        Scanner sc = new Scanner(System.in);
        int input;
        do{
            System.out.println("Which account do you want to open? ");
            System.out.println("1 - Savings");
            System.out.println("2 - Checking");
            input = sc.nextInt();
            if(input < 1 || input > 2){
                System.out.println("Invalid Entry!! TRY AGAIN!!");
            }
        }while(input < 1 || input > 2);
        return input;
    }

    public static void menuInitial(){
        Scanner sc = new Scanner(System.in);
        int input = 0;
        do {
            System.out.println("------------------------Choose from these choices-----------------------");
            System.out.println("1 - New User");
            System.out.println("2 - Login");
            System.out.println("3 - Exit");
            input = sc.nextInt();
            if(input < 1 || input > 3){
                System.out.println("Invalid Entry!! TRY AGAIN!!");
            }
        }while(input < 1 || input > 3);
        switch(input){
            case 1:////new account creation//////
                newAccountCreation();
                break;
            case 2:////already registered users
                accountOperations();
                break;
            case 3:
                sc.close();
                break;
        }
    }
}

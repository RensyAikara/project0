package dev.aik.userInterfaces;

import dev.aik.daos.CheckingAccountDao;
import dev.aik.daos.CheckingAccountDaoImpl;
import dev.aik.daos.SavingsAccountDao;
import dev.aik.daos.SavingsAccountDaoImpl;
import dev.aik.models.CheckingAccount;
import dev.aik.models.SavingsAccount;

import java.util.List;
import java.util.Scanner;

public class uiFunctions {
//Operations performed on the UI for Checking account, once logged in(View balance, deposit, withdraw, view transaction history)
    public static void savingsOperations( String savAcNumber, int loggedInUserId, String dateTime){
        Scanner sc = new Scanner(System.in);
        SavingsAccountDao savingDao = new SavingsAccountDaoImpl();
        System.out.println("Choose from these choices");
        System.out.println("1 - View Balance");
        System.out.println("2 - Deposit");
        System.out.println("3 - Withdraw");
        System.out.println("4 - View Transaction History");
        System.out.println("5 - Transfer to Checking");
        System.out.println("6 - Logout");
        int input = sc.nextInt();
        switch (input){
            case 1:     ///////////// view balance////////////
                double balance = savingDao.viewSavingBalance(loggedInUserId);
                System.out.println("Your Balance is: $" +balance);
                savingsOperations(savAcNumber, loggedInUserId, dateTime);
                break;
            case 2:        ///////////deposit money//////////
                System.out.println("Enter deposit amount: ");
                Double depositAmount = sc.nextDouble();
                while(depositAmount<=0){
                    System.out.println("Enter valid amount: ");
                    depositAmount = sc.nextDouble();
                }
                double updatedBalance = savingDao.viewSavingBalance(loggedInUserId) + depositAmount;
                System.out.println("Updated balance is: " +updatedBalance);
                double withdrawAmount0 = 0;
                SavingsAccount newDeposit = new SavingsAccount(savAcNumber,updatedBalance,depositAmount,withdrawAmount0,dateTime,loggedInUserId);
                boolean resultDeposit = savingDao.depositSavings(newDeposit);
                if(resultDeposit == true){
                    System.out.println("Deposited " +depositAmount + " to your account...");
                    savingsOperations(savAcNumber, loggedInUserId, dateTime);
                }
                break;
            case 3:        //////////withdraw amount////////
                System.out.println("Enter amount to withdraw: ");
                Double withdrawAmount = sc.nextDouble();
                while(withdrawAmount<=0){
                    System.out.println("Enter valid amount: ");
                    withdrawAmount = sc.nextDouble();
                }
                updatedBalance = savingDao.viewSavingBalance(loggedInUserId) - withdrawAmount;
                if(withdrawAmount > savingDao.viewSavingBalance(loggedInUserId)){
                    System.out.println("Error!! Trying to withdraw an amount higher than balance");
                    savingsOperations(savAcNumber, loggedInUserId, dateTime);
                }else {
                    double depositAmount0 = 0;
                    System.out.println("Updated balance is: " +updatedBalance);
                    SavingsAccount newWithdraw = new SavingsAccount(savAcNumber, updatedBalance, depositAmount0, withdrawAmount, dateTime, loggedInUserId);
                    boolean resultWithdraw = savingDao.withdrawSavings(newWithdraw);
                    if(resultWithdraw == true){
                        System.out.println("Debited " +withdrawAmount + " from your account...");
                        savingsOperations(savAcNumber, loggedInUserId, dateTime);
                    }
                }
                break;
            case 4:        /////////transaction history/////////
                System.out.println("View transaction History: ");
                List<SavingsAccount> transactions = savingDao.transactionHistorySavings(loggedInUserId);
                System.out.println("-----------------------Here is your transaction history: ");
                for (SavingsAccount sa : transactions) {
                    System.out.println(sa);
                }
                savingsOperations(savAcNumber, loggedInUserId, dateTime);
                break;
            case 5:        //////////transfer amount///////////
                System.out.println("Enter amount to transfer to Checking: ");
                double transferAmount = sc.nextDouble();
                while(transferAmount<=0){
                    System.out.println("Enter valid amount: ");
                    transferAmount = sc.nextDouble();
                }
                if(transferAmount > savingDao.viewSavingBalance(loggedInUserId)){
                    System.out.println("Error!! Trying to transfer amount higher than balance");
                    savingsOperations(savAcNumber, loggedInUserId, dateTime);
                }else{
                    double depositAmount1 = 0;

                    CheckingAccountDao checkingAc = new CheckingAccountDaoImpl();
                    String checkingAcNumber = checkingAc.checkingAccountFound(loggedInUserId);
                    if(checkingAcNumber == "") {
                        System.out.println("No savings account found ");
                        savingsOperations(savAcNumber, loggedInUserId, dateTime);
                    }
                    else{
                        updatedBalance = savingDao.viewSavingBalance(loggedInUserId) - transferAmount;
                        System.out.println("Updated balance is: " +updatedBalance);
                        SavingsAccount newWithdraw4 = new SavingsAccount(savAcNumber, updatedBalance, depositAmount1, transferAmount, dateTime, loggedInUserId);
                        boolean resultWithdraw = savingDao.withdrawSavings(newWithdraw4);
                        if(resultWithdraw == true){
                            double updatedCheckingBalance = checkingAc.viewCheckingBalance(loggedInUserId) + transferAmount;
                            double withdrawAmount2 = 0;
                            CheckingAccount newtransfer3 = new CheckingAccount(checkingAcNumber,updatedCheckingBalance,transferAmount,withdrawAmount2,dateTime,loggedInUserId);
                            boolean resultTransfer = checkingAc.depositChecking(newtransfer3);
                            if(resultTransfer == true) {
                                System.out.println("-----------------Transfered " + transferAmount + " from checking to savings---------------------");
                            }
                        }
                    }
                }
                savingsOperations(savAcNumber, loggedInUserId, dateTime);
                break;
            case 6:
                System.out.println("----------------------Thanks for choosing Rev Bank!!!!!!!!!!!!--------------------------");
                sc.close();
                break;
            default:
                System.out.println("--------Invalid Entry. Choose a valid one------");
                savingsOperations(savAcNumber, loggedInUserId, dateTime);
                break;
        }
    }
//Operations performed on the UI for Checking account, once logged in(View balance, deposit, withdraw, view transaction history)
    public static void checkingOperations(String checkAcNumber, int loggedInUserId, String dateTime){
        Scanner sc = new Scanner(System.in);
        CheckingAccountDao checkingDao = new CheckingAccountDaoImpl();
        System.out.println("Choose from these choices");
        System.out.println("1 - View Balance");
        System.out.println("2 - Deposit");
        System.out.println("3 - Withdraw");
        System.out.println("4 - View Transaction History");
        System.out.println("5 - Transfer to Savings");
        System.out.println("6 - Logout");
        int input = sc.nextInt();
        switch (input){
            case 1:     ///////////// view balance////////////
                double balance = checkingDao.viewCheckingBalance(loggedInUserId);
                System.out.println("Your Balance is: $" +balance);
                checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                break;
            case 2:        ///////////deposit money//////////
                System.out.println("Enter deposit amount: ");
                Double depositAmount = sc.nextDouble();
                while(depositAmount<=0){
                    System.out.println("Enter valid amount: ");
                    depositAmount = sc.nextDouble();
                }
                double updatedBalance = checkingDao.viewCheckingBalance(loggedInUserId) + depositAmount;
                System.out.println("Updated balance is: " +updatedBalance);
                double withdrawAmount0 = 0;
                CheckingAccount newDeposit = new CheckingAccount(checkAcNumber,updatedBalance,depositAmount,withdrawAmount0,dateTime,loggedInUserId);
                boolean resultDeposit = checkingDao.depositChecking(newDeposit);
                if(resultDeposit == true){
                    System.out.println("Deposited " +depositAmount + " to your account...");
                    checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                }
                break;
            case 3:        //////////withdraw amount////////
                System.out.println("Enter amount to withdraw: ");
                Double withdrawAmount = sc.nextDouble();
                while(withdrawAmount<=0){
                    System.out.println("Enter valid amount: ");
                    withdrawAmount = sc.nextDouble();
                }
                updatedBalance = checkingDao.viewCheckingBalance(loggedInUserId) - withdrawAmount;
                if(withdrawAmount > checkingDao.viewCheckingBalance(loggedInUserId)){
                    System.out.println("Error!! Trying to withdraw an amount larger than balance");
                    checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                }else {
                    double depositAmount0 = 0;
                    CheckingAccount newWithdraw = new CheckingAccount(checkAcNumber, updatedBalance, depositAmount0, withdrawAmount, dateTime, loggedInUserId);
                    boolean resultWithdraw = checkingDao.withdrawChecking(newWithdraw);
                    if(resultWithdraw == true){
                        System.out.println("Debited " +withdrawAmount + " from your account...");
                        checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                    }
                }
                break;
            case 4:        /////////transaction history/////////
                System.out.println("View transaction History: ");
                List<CheckingAccount> transactions = checkingDao.transactionHistoryChecking(loggedInUserId);
                System.out.println("-----------------------Here is your transaction history: ");
                for (CheckingAccount sa : transactions) {
                    System.out.println(sa);
                }
                checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                break;
            case 5:        //////////transfer amount///////////
                System.out.println("Enter amount to transfer to Savings: ");
                double transferAmount = sc.nextDouble();
                while(transferAmount<=0){
                    System.out.println("Enter valid amount: ");
                    transferAmount = sc.nextDouble();
                }
                if(transferAmount > checkingDao.viewCheckingBalance(loggedInUserId)){
                    System.out.println("Error!! Trying to transfer amount higher than balance");
                    checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                }else{
                    double depositAmount1 = 0;

                    SavingsAccountDao savingAc = new SavingsAccountDaoImpl();
                    String savingsAcNumber = savingAc.savingsAccountFound(loggedInUserId);
                    if(savingsAcNumber == "") {
                        System.out.println("No savings account found ");
                        checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                    }
                    else{
                        updatedBalance = checkingDao.viewCheckingBalance(loggedInUserId) - transferAmount;
                        System.out.println("Updated balance is: " +updatedBalance);
                        CheckingAccount newWithdraw1 = new CheckingAccount(checkAcNumber, updatedBalance, depositAmount1, transferAmount, dateTime, loggedInUserId);
                        boolean resultWithdraw = checkingDao.withdrawChecking(newWithdraw1);
                        if(resultWithdraw == true){
                            double updatedSavingBalance = savingAc.viewSavingBalance(loggedInUserId) + transferAmount;
                            double withdrawAmount2 = 0;
                            SavingsAccount newtransfer3 = new SavingsAccount(savingsAcNumber,updatedSavingBalance,transferAmount,withdrawAmount2,dateTime,loggedInUserId);
                            boolean resultTransfer = savingAc.depositSavings(newtransfer3);
                            if(resultTransfer == true) {
                                System.out.println("-----------------Transfered " + transferAmount + " from checking to savings---------------------");
                            }
                        }
                    }
                }
                checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                break;
            case 6:
                System.out.println("----------------------Thanks for choosing Rev Bank!!!!!!!!!!!!--------------------------");
                sc.close();
                break;
            default:
                System.out.println("--------Invalid Entry. Choose a valid one------");
                checkingOperations(checkAcNumber, loggedInUserId, dateTime);
                break;
        }
    }
}

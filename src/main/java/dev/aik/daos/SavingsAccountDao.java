package dev.aik.daos;

import dev.aik.models.SavingsAccount;

import java.util.List;

public interface SavingsAccountDao {
    public String generateSavingsAccountNumber(Long phoneNumber);
    public boolean createSavingsAccount(SavingsAccount newAccount);
    public double viewSavingBalance(int userId);
    public boolean depositSavings(SavingsAccount newDeposit);
    public boolean withdrawSavings(SavingsAccount newWithdraw);
    public boolean transferSavings();
    public List<SavingsAccount> transactionHistorySavings(int userId);
    public String savingsAccountFound(int userId);
}

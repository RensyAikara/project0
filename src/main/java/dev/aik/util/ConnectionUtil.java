package dev.aik.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static Connection connection;

    public static Connection getConnection() throws SQLException {
        if(connection == null || connection.isClosed()){
            String url = "jdbc:postgresql://training-db.cimgpetk601j.us-east-2.rds.amazonaws.com:5432/postgres?currentSchema=project0";
            final String USERNAME = System.getenv("USERNAME");
            final String PASSWORD = System.getenv("PASSWORD");
            connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
        }
        return connection;
    }
}
